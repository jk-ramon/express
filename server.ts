import { PrismaClient } from "@prisma/client"

const express = require('express')
const app = express()
const port = 3001

const prisma = new PrismaClient()

app.use(express.json())

app.get('/', (req: any, res: any) => {
  res.send('Hello World!')
})

app.get('/get-orders', async (req: any, res: any ) => {
  const Orders = await prisma.order.findMany({
    include: {
      colli: true
    }
})

  res.send(Orders)
})

app.post('/create-order', async (req: any, res: any) => {
  const order = await prisma.order.create({
    data: {
      destination: req.body.destination,
      origin: req.body.origin,
      agent: req.body.agent,
      totalPieces: 0,
      contactPerson: req.body.contactPerson,
    }
  })

  let totalPieces = 0


  req.body.colli.map(async (c: any) => {

    totalPieces = totalPieces + c.pieces

    await prisma.colli.create({
      data:{
        length: c.length,
        height: c.height,
        weight: c.weight,
        pieces: c.pieces,
        width: c.width,
        orderId: order.id
      }
    })
  })

  await prisma.order.update({
    data: {
      totalPieces: totalPieces
    }, where: {
      id: order.id
    }
  })

  res.send({id: order.id, message: "Order has been created"})
})

app.post('/validate-email', (req: any, res: any) => {
  const mailbody = req.body.mailbody
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
